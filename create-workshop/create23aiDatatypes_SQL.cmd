rem ECHO OFF
REM Parameter G is path to git repository containing base scripts
set G=%123ai\
ECHO Creating 23aiDatatypes_SQL.sql
REM echo Using Git-Repository %G%
copy %G%23aiIfNotExists.sql+%G%23aiMisc.sql+%G%23aiValuesContructor.sql+%G%23aiBoolean.sql+%G%23aiGroupBy.sql+%G%23aiDirectJoins.sql+%G%23aiDefaultOnNull.sql+%G%23aiSQLAnalysis.sql+%G%23aiSubsumptions.sql+%G%23aiANSI-MVs.sql ..\23aiDatatypes_SQL.sql