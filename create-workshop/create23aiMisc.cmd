ECHO OFF
REM Parameter G is path to git repository containing base scripts
set G=%123ai\
ECHO Creating 23aiMisc.sql
REM echo Using Git-Repository %G%
copy %G%23aiTouchOnceRestriction.sql+%G%23aiRenameLobSegments.sql+%G%23aiInterval-Timestamp-Date.sql+%G%23aiTranspiler.sql+%G%23aiMacros.sql+%G%23aiFuzzyMatch.sql+%G%23aiPLSQL.sql ..\23aiMisc.sql