ECHO OFF
REM Parameter G is path to git repository containing base scripts
set G=%123ai\
ECHO Copying 23aiSQLFirewall.sql
REM echo Using Git-Repository %G%
copy "%G%23aiSQLFirewall_app_user.sql" "..\23aiSQLFirewall_app_user.sql"
copy "%G%23aiSQLFirewall_scott.sql" "..\23aiSQLFirewall_scott.sql"
copy "%G%23aiSQLFirewall_sys.sql" "..\23aiSQLFirewall_sys.sql"