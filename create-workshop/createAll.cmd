REM ECHO ON
REM Parameter R is path to git repository root containing base scripts
set R=D:\Arnes\git\oracle-db-examples\
echo Using Git Repositoy %R%
call create23aiDatatypes_SQL.cmd %R%
call create23aiDeveloperRole.cmd %R%
call create23aiDomains-Annotations.cmd %R%
call create23aiJSON.cmd %R%
call create23aiLockFreeReservations.cmd %R%
call create23aiMisc.cmd %R%
call create23aiMLE_JavaScript.cmd %R%
call create23aiPropertyGraph.cmd %R%
call create23aiSchemaLevelPrivs.cmd %R%
call create23aiSQLFirewall.cmd %R%
call create21cSQL-PLSQL.cmd %R%
call create23aiUbiquitousSearch.cmd %R%
call create23aiPriorityTrans.cmd %R%
call create23aiSessionlessTransactions.cmd %R%
echo --- Done ---