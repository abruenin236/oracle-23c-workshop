------------------------------------
--- JSON Relational Duality View ---
------------------------------------

SET LONG 10000

--- create a simple table and add two rows

CREATE TABLE student(
  sid   NUMBER,
  name  VARCHAR(128),
  major VARCHAR(128),
  CONSTRAINT pk_student PRIMARY KEY (sid)
);

INSERT INTO student VALUES
      (1, 'Student A', 'Computer Science'),
      (2, 'Student B', 'Biology');
COMMIT;

select * from student;

--- create Duality View (here not via GraphQL but just as simple JSON Object)
CREATE OR REPLACE JSON DUALITY VIEW studentV AS 
SELECT JSON {
    '_id'   : sid,
    'name'  : name,
    'major' : major 
            }
FROM student WITH (update, insert, delete);

--- View as JSON (copy student2)
--- injected metadata for optimistic locking (ETAG & kind of Timestamp)
SELECT json_serialize(data pretty) FROM studentV;

--- View relational und update with SQL
select * from studentV;

UPDATE student SET name = 'Hermann' WHERE sid = 1;
COMMIT;

--- see results relational and as document 
SELECT name FROM student;
SELECT json_serialize(data pretty) FROM studentV;


-- update via document (use copy from above) -- etag to ensure current version
UPDATE studentV sv SET data = '{
  "_id" : 2,
  "_metadata" :
  {
    "etag" : "7C5064E638D6FF9AC2E4D2D5B4877EA6",
    "asof" : "00000000008795B8"
  },
  "name" : "Arne",
  "major" : "Biology"
}'
WHERE sV.data."_id" = 2;


--- see results relational and as document 
SELECT json_serialize(data pretty) FROM studentV;
SELECT name FROM student;


-- update another time, etag mismatch!
UPDATE studentV sv SET data = '{
  "_id" : 2,
  "_metadata" :
  {
    "etag" : "7C5064E638D6FF9AC2E4D2D5B4877EA6",
    "asof" : "00000000014669BF"
  },
  "name" : "Beda",
  "major" : "Biology"
}'
WHERE sV.data."_id" = 2;


----------------------------
--- More complex example ---
----------------------------

-- expand data model
--- teachers with classes and schedules for the students

CREATE TABLE teacher(
  tid    NUMBER,
  name   VARCHAR(128),
  salary NUMBER,
  CONSTRAINT pk_teacher PRIMARY KEY (tid)
);

INSERT INTO teacher VALUES
      (1, 'Teacher A', 100),
      (2, 'Teacher B', 150);

CREATE TABLE class(
  cid       NUMBER,
  name      VARCHAR2(128),
  room      VARCHAR2(128),
  teacherId NUMBER,
  CONSTRAINT pk_class PRIMARY KEY (cid),
  CONSTRAINT fk_class FOREIGN KEY (teacherId) REFERENCES teacher(tid)
);

INSERT INTO class VALUES 
      (1, 'Algorithms', 'Room 1', 1),
      (2, 'Data Structures', 'Room 2', 1),
      (3, 'Cell Physiology', 'Room 1', 2);

CREATE TABLE schedule (
  schedId NUMBER,
  sid     NUMBER,
  cid     NUMBER,
  CONSTRAINT pk_student_class PRIMARY KEY (schedId),
  CONSTRAINT fk_student_class1 FOREIGN KEY (sid) REFERENCES student(sid),
  CONSTRAINT fk_student_class2 FOREIGN KEY (cid) REFERENCES class(cid)
);

INSERT INTO schedule VALUES 
   (1,1,1),(2,1,2),(3,2,3);
COMMIT;


-- view Students and their courses
SELECT * FROM teacher;
SELECT * FROM student;
SELECT * FROM class;
SELECT * FROM schedule;


-- more complex Duality View - let allow change the schedules
CREATE OR REPLACE JSON DUALITY VIEW StudentScheduleV AS
Student 
{
  _id : sid
  name : name
  classes :  schedule  @insert @update @delete
  {
    schedId : schedId
    class @unnest
    {
      classId   : cid
      className : NAME
      location  : ROOM
      teacher  @unnest
      {
         teacherId : tid
         name      : NAME
      }
    } 
  }
} ;


-- copy "Herrmanns" metadata and update via Duality view to add another course
select * from SCHEDULE;

SELECT json_serialize(data pretty) FROM StudentScheduleV;

UPDATE StudentScheduleV ssv SET data = '{
  "_metadata" :
  {
    "etag" : "5D4AE5D3E5D129267135EF70BEA8B4F2",
    "asof" : "000000000087971E"
  },
  "_id" : 1,
  "name" : "Hermann",
  "classes" :
  [
    {
      "schedId" : 1,
      "classId" : 1,
      "className" : "Algorithms",
      "location" : "Room 1",
      "teacherId" : 1,
      "name" : "Teacher A"
    },
    {
      "schedId" : 2,
      "classId" : 2,
      "className" : "Data Structures",
      "location" : "Room 2",
      "teacherId" : 1,
      "name" : "Teacher A"
    },
    {
      "schedId" : 4,
      "classId" : 3,
      "className" : "Cell Physiology",
      "location" : "Room 1",
      "teacherId" : 2,
      "name" : "Teacher B"
    }    
  ]
}'
WHERE ssv.data."_id" = 1;


SELECT * FROM STUDENTSCHEDULEV;
select * from SCHEDULE;


--------------------
--- FLEX-Columns ---
--------------------

--- create a table with a JSON Type column and insert a row

CREATE TABLE flextable 
   (id_col NUMBER PRIMARY KEY,
    col1   VARCHAR2(10),
    col2   VARCHAR2(10),
    myflex JSON(OBJECT));
    
INSERT INTO flextable VALUES (1,'first','entry',null);
COMMIT;

select * from flextable;
 
--- create a simple Duality view which maps the JSON column as flex 
CREATE OR REPLACE JSON RELATIONAL DUALITY VIEW flexview AS
  flextable @insert @update @delete
    {_id    : id_col,
     field1 : col1,
     field2 : col2,
     myflex @flex};


--- check view & table (and copy the document)
SELECT json_serialize(data pretty) FROM flexview;

SELECT * FROM flexview;


--- update the view with no mapped attributes and have a look into the table
UPDATE flexview fv SET data = '{
  "_id" : 1,
  "_metadata" :
  {
    "etag" : "876292EC3C89EBB64A1A4CC60F2DB7C9",
    "asof" : "00000000008797A8"
  },
  "field1" : "second",
  "field2" : "entry",
  "shoesize"  : "42",
  "toes"      : "10"}'
WHERE fv.data."_id" = 1;

SELECT * FROM flextable;

--- rollback and just to remember JSON_TRANSFORM let's try this (where the etag is correct by definition)
ROLLBACK;
UPDATE flexview fv SET data = JSON_TRANSFORM (data,
                                                SET '$.field1' = 'second',
                                                SET '$.shoesize' = '43',
                                                SET '$.fingers' = '10')
 WHERE fv.data."_id" = 1;

SELECT * FROM flextable;


