----------------------------------------
--- Startpoint is a 23ai Installation ---
----------------------------------------

--- run as SYS from sqlcl

SET ECHO OFF
CLEAR SCREEN

------------------------------
--- (RE-)CREATE DEMO USERS ---
------------------------------

@setup-23ai-Demo-users.sql

-------------------
--- Prepare MLE ---
-------------------

@23aiMLE_JavaScript-setup.sql

-----------------------------
--- Prepare Vector Search ---
-----------------------------

@23aiVectorDB-setup.sql


---------------------------
--- Setup as user scott ---
---------------------------

conn scott/oracle@freepdb1

----------------------------------------------------
--- run utlxplan.sql for showing execution plans ---
----------------------------------------------------

@setup-xplan.sql

--------------------------
--- Prepare Graph Data ---
--------------------------

@23aiPropertyGraph-setup.sql

----------------------------------------------------
--- Prepare Table for Ubiquitous Database Search ---
----------------------------------------------------

@23aiUbiquitousSearch-setup.sql

