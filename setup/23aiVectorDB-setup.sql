PROMPT ###### 23aiVectorDB-setup.sql ######

--- SYS in PDB

CREATE OR REPLACE DIRECTORY vector_dir AS '/home/oracle/Demo/Vector';

/* Use dedicated user
CREATE BIGFILE TABLESPACE tbs_vector DATAFILE SIZE 256M AUTOEXTEND ON MAXSIZE 2G;

CREATE USER vector_user IDENTIFIED BY "vector_user"
DEFAULT TABLESPACE tbs_vector TEMPORARY TABLESPACE temp
QUOTA UNLIMITED ON tbs_vector;

GRANT db_developer_role TO vector_user;
GRANT CREATE MINING MODEL TO vector_user;

GRANT READ, WRITE ON DIRECTORY vector_dir TO vector_user;
EXIT
*/

/* Use user scott */

GRANT CREATE MINING MODEL TO scott;
GRANT READ ON DIRECTORY vector_dir TO scott;


-- download model into directory
-- wget https://objectstorage.eu-frankfurt-1.oraclecloud.com/n/fro8fl9kuqli/b/AIVECTORS/o/all-MiniLM-L6-v2.onnx





