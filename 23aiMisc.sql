------------------------------------------------------------
--- Removal of Touch-Once Restriction after Parallel DML ---
------------------------------------------------------------

SELECT * FROM v$version;

CREATE TABLE otr AS SELECT * FROM all_objects;

SELECT count(*) FROM otr;

ALTER SESSION enable parallel dml;

INSERT /*+ parallel(otr 8) */ INTO otr
   SELECT * FROM otr;
   
-- Would throw an ORA-12838 on older versions
-- Need to commit or rollback before referencing a table modified by the parallel DML
-- But works with 23c
SELECT count(*) FROM otr;

ROLLBACK;



---------------------------
--- RENAME LOB Segments ---
---------------------------


CREATE TABLE mylob (id     NUMBER,
                    doc    BLOB);


SELECT table_name, column_name, segment_name FROM user_lobs WHERE table_name ='MYLOB';


ALTER TABLE mylob RENAME LOB(doc) SYS_LOB0000071640C00002$$ TO meaningfulname;


SELECT table_name, column_name, segment_name FROM user_lobs WHERE table_name ='MYLOB';

-- before you had to do a ALTER TABLE MOVE for that


---------------------------------------------------
--- FLOOR & CEIL for DATE, TIMESTAMP & INTERVAL ---
---------------------------------------------------

SELECT sysdate;
ALTER SESSION SET nls_date_format='DD.MM.YYYY HH24:MI:SS';
ALTER SESSION SET nls_timestamp_format='DD.MM.YYYY HH24:MI:SS';

WITH mydates (what,mydate) AS
     (VALUES
          ('sysdate',sysdate),
          ('morning',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 11:11:11' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('grandpa lunchtime',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 12:00:00' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('afternoon',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 15:55:55' , 'DD.MM.YYYY HH24:MI:SS' ))
     )
SELECT what, mydate, trunc(mydate), round(mydate), floor(mydate), ceil(mydate) FROM mydates; 

-- also with HH24
WITH mydates (what,mydate) AS
     (VALUES
          ('sysdate',sysdate),
          ('morning',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 11:11:11' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('grandpa lunchtime',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 12:00:00' , 'DD.MM.YYYY HH24:MI:SS' )),
          ('afternoon',to_date(to_char(trunc(sysdate),'DD.MM.YYYY')||' 15:55:55' , 'DD.MM.YYYY HH24:MI:SS' ))
     )
SELECT what, mydate, trunc(mydate,'HH24'), round(mydate,'HH24'), floor(mydate,'HH24'), ceil(mydate,'HH24') FROM mydates; 

-- and with interval
WITH mytimes (what,mytime) AS
     (VALUES
          ('    100m Worldrecord       ',to_dsinterval('0 00:00:09.58')),
          (' 10.000m Worldrecord       ',to_dsinterval('0 00:26:11')),
          ('Marathon Worldrecord       ',to_dsinterval('0 02:21:09')),
          ('Tortour de Ruhr 100mi, 2022',to_dsinterval('0 16:48:17'))
     )
SELECT what, mytime, trunc(mytime), round(mytime), floor(mytime), ceil(mytime) FROM mytimes; 

-- also with 24hrs
WITH mytimes (what,mytime) AS
     (VALUES
          ('    100m Worldrecord       ',to_dsinterval('0 00:00:09.58')),
          (' 10.000m Worldrecord       ',to_dsinterval('0 00:26:11')),
          ('Marathon Worldrecord       ',to_dsinterval('0 02:21:09')),
          ('Tortour de Ruhr 100mi, 2022',to_dsinterval('0 16:48:17'))
     )
SELECT what, mytime, trunc(mytime,'HH24'), round(mytime,'HH24'), floor(mytime,'HH24'), ceil(mytime,'HH24') FROM mytimes; 


-- Week starts Monday
ALTER SESSION SET NLS_TERRITORY=Germany;
WITH mysysdate as (
    select 
        floor(sysdate,'d') as floordate,
        trunc(sysdate,'d') as truncdate
    )
select floordate, to_char(floordate, 'DY'), truncdate, to_char(truncdate, 'DY')  
from mysysdate;


-------------------------------------
--- Aggregation of INTERVAL Types ---
-------------------------------------

CREATE TABLE Processes
  (	id        NUMBER GENERATED ALWAYS AS IDENTITY,
    ptitle    VARCHAR2(30),
    pstart    TIMESTAMP,
    pend      TIMESTAMP,
    pduration INTERVAL day to second GENERATED ALWAYS AS (pend - pstart) VIRTUAL
   );
   
INSERT INTO processes (ptitle,pstart,pend) VALUES
    ('Process A',trunc(sysdate)+540/1440,trunc(sysdate)+1080/1440),
    ('Process B',trunc(sysdate)+640/1440,trunc(sysdate)+1000/1440),
    ('Process C',trunc(sysdate)+800/1440,trunc(sysdate)+910/1440),
    ('Process D',trunc(sysdate)+800/1440,trunc(sysdate)+1200/1440),
    ('Process E',trunc(sysdate)+840/1440,trunc(sysdate)+1080/1440);
    
SELECT * FROM processes;

SELECT sum(pduration), avg(pduration) FROM processes;


--------------------------------
--- Automatic SQL Transpiler ---
--------------------------------

CREATE OR REPLACE FUNCTION datetostring_f (p_hiredate DATE) RETURN VARCHAR2 IS
BEGIN
   RETURN to_char(p_hiredate,'YYYY.MM.DD');
END;
/

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'ON';

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'OFF';


------------------
--- SQL Macros ---
------------------


-- It's a new, simpler way to encapsulate complex processing logic directly within SQL. 
-- SQL Macros allow developers to encapsulate complex processing within a new structure called a "macro",
--- which can then be used within SQL statement. 
-- Essentially there two types of SQL Macros: SCALAR and TABLE. 
--- SCALAR expressions can be used in SELECT list, WHERE/HAVING, GROUP BY/ORDER BY clauses  
--- TABLE expressions used in a FROM-clause

/* -- SCALAR Macro (vs. Function)
CREATE OR REPLACE FUNCTION datetostring_f (p_hiredate DATE) RETURN VARCHAR2 IS
BEGIN
  RETURN to_char(p_hiredate,'YYYY.MM.DD');
END;
/ */

-- see type SCALAR - default is TABLE
CREATE OR REPLACE FUNCTION datetostring_m (p_hiredate DATE) RETURN VARCHAR2 sql_macro(scalar) IS
BEGIN
  RETURN q'{to_char(p_hiredate,'YYYY.MM.DD')}';
END;
/

SELECT to_char(hiredate,'YYYY'), datetostring_f(hiredate), datetostring_m(hiredate) FROM emp; 

SELECT count(*) 
 WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;
 
SELECT * FROM dbms_xplan.display_cursor();

SELECT count(*)
 WHERE datetostring_m ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

--- TABLE Macro
CREATE OR REPLACE FUNCTION total_salary(department VARCHAR2 DEFAULT NULL,
                                        job        VARCHAR2 DEFAULT NULL) 
   RETURN CLOB SQL_MACRO IS
BEGIN
  RETURN q'{
   SELECT d.deptno, d.dname, e.job, sum(e.sal) AS SalSummary 
     FROM emp e, dept d
    WHERE e.deptno=d.deptno
      AND d.dname = coalesce(total_salary.department,d.dname)
      AND e.job   = coalesce(total_salary.job,e.job)
     GROUP BY d.deptno, d.dname, e.job
  }';
END;
/
 
SELECT * FROM total_salary('RESEARCH','CLERK'); 

SELECT * FROM total_salary('RESEARCH');

SELECT * FROM total_salary(job=>'CLERK');

SELECT * FROM total_salary();



--- optional
CREATE OR REPLACE FUNCTION myselect 
    (n NUMBER, 
     t dbms_tf.table_t,
     c dbms_tf.columns_t) 
  RETURN VARCHAR2 SQL_MACRO IS
BEGIN
   RETURN 'SELECT * FROM t ORDER BY ' || c(1) || ' FETCH FIRST n ROWS ONLY';
END;
/

select * from myselect(3,emp,columns(ename));

select * from myselect(2,dept,columns(loc));

select * from myselect(10,user_objects,columns(object_name));



CREATE TABLE meeting_attendees (  
  attendee_id  NUMBER NOT NULL,  
  start_date   DATE NOT NULL,  
  end_date     DATE,  
  PRIMARY KEY( attendee_id, start_date )  
)
/

INSERT INTO meeting_attendees VALUES 
    ( 1, trunc(sysdate) + 9/24, trunc(sysdate) + 10/24 ),
    ( 1, trunc(sysdate) + 10/24, trunc(sysdate) + 10.5/24 ),
    ( 1, trunc(sysdate) + 11/24, trunc(sysdate) + 13/24 ),
    ( 1, trunc(sysdate) + + 16/24, trunc(sysdate) + 17/24 ),
    ( 2, trunc(sysdate) + 9/24, trunc(sysdate) + 12/24 ),
    ( 2, trunc(sysdate) + 12.5/24, trunc(sysdate) + 13/24 ),
    ( 2, trunc(sysdate) + 13/24, trunc(sysdate) + 14/24 ),
    ( 2, trunc(sysdate) + 15/24, trunc(sysdate) + 17/24 );

SELECT * FROM meeting_attendees  
ORDER BY attendee_id, start_date, end_date
/

SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')
  FROM meeting_attendees MATCH_RECOGNIZE (  
    ORDER BY start_date, end_date  
    MEASURES  
      max ( end_date ) start_gap,   
      next ( start_date ) end_gap
    ALL ROWS PER MATCH   
    PATTERN ( ( gap | {-no_gap-} )+ )  
    DEFINE gap AS max ( end_date ) < next ( start_date )  
  )
/

CREATE OR REPLACE FUNCTION fg (  
  tab            dbms_tf.table_t,   
  date_cols      dbms_tf.columns_t  
)  
  RETURN VARCHAR2   
  SQL_MACRO   
AS  
BEGIN  
  RETURN 'tab MATCH_RECOGNIZE (   
             ORDER BY ' || fg.date_cols ( 1 ) || ', ' || fg.date_cols ( 2 ) || '    
             MEASURES     
               max ( ' || fg.date_cols ( 2 ) || ' ) start_gap,   
               next ( ' || fg.date_cols ( 1 ) || ' ) end_gap  
               ALL ROWS PER MATCH  
               PATTERN ( ( gap | {-no_gap-} )+ )    
               DEFINE     
                  gap AS  max ( ' || fg.date_cols ( 2 ) || ' ) < (   
                  next ( ' || fg.date_cols ( 1 ) || ' )  
                  )    
              )';  
END fg; 
/

SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')  
FROM   fg (   
  meeting_attendees,   
  COLUMNS ( start_date, end_date )   
)
/

CREATE TABLE other_table (  
  person     VARCHAR2(10) NOT NULL,  
  timebegin  DATE,  
  timeend    DATE 
)
/

INSERT INTO other_table VALUES 
    ( 'Rainer', trunc(sysdate) + 9/24, trunc(sysdate) + 10/24 ),
    ( 'Rainer', trunc(sysdate) + 10/24, trunc(sysdate) + 10.5/24 ),
    ( 'Rainer', trunc(sysdate) + 11/24, trunc(sysdate) + 13/24 ),
    ( 'Rainer', trunc(sysdate) + 16/24, trunc(sysdate) + 17/24 ),
    ( 'Marcel', trunc(sysdate) + 12.5/24, trunc(sysdate) + 13/24 ),
    ( 'Marcel', trunc(sysdate) + 13/24, trunc(sysdate) + 14/24 ),
    ( 'Marcel', trunc(sysdate) + 15/24, trunc(sysdate) + 17/24 );  


SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')  
FROM   fg (   
  other_table,   
  COLUMNS ( timebegin, timeend )   
)
/


---------------------------------------------------------------------------------
--- Data Quality Operators in Oracle Database - Fuzzy match & Phonetic Encode ---
---------------------------------------------------------------------------------

CREATE TABLE mytab (id      NUMBER,
                    value1  VARCHAR2(20),
                    value2  VARCHAR2(20));
                    
INSERT INTO mytab VALUES
  (1, 'Rainer Willems', 'Rainer Willens'),
  (2, 'Christoph Blessing', 'Christof Blässing'),
  (3, 'Marlon Deus', 'Marlon Brando'),
  (4, 'Wolfgang Thiem', 'Wolfgang Team'),
  (5, 'Thomas Gorsitzke', 'Arne Brüning'),
  (6, 'Ute Müller','Ute Müller');
  
COMMIT;                    
                    
SELECT value1, value2,
       fuzzy_match(levenshtein, value1, value2) AS levenshtein,
       fuzzy_match(damerau_levenshtein, value1, value2) AS damerau_levenshtein,
       fuzzy_match(jaro_winkler, value1, value2) AS jaro_winkler,
       fuzzy_match(bigram, value1, value2) AS bigram,
       fuzzy_match(trigram, value1, value2) AS trigram,
       fuzzy_match(whole_word_match, value1, value2) AS whole_word_match,
       fuzzy_match(longest_common_substring, value1, value2) AS longest_common_substring
FROM   mytab;

--- UNSCALED, RELATE_TO_SHORTER & EDIT_TOLERANCE 

SELECT value1, value2,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 1) AS whole_word_match_1,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 25) AS whole_word_match_25,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 75) AS whole_word_match_75,
       fuzzy_match(whole_word_match, value1, value2, EDIT_TOLERANCE 100) AS whole_word_match_100,
       fuzzy_match(whole_word_match, value1, value2) AS whole_word_match
FROM   mytab;


---------------------------------------------
--- SQL UPDATE RETURN Clause Enhancements ---
---------------------------------------------

set serveroutput on

-- RETURNING old and new
DECLARE
   p_empno  emp.empno%TYPE := 7499;
   v_sal_o  emp.sal%TYPE;
   v_sal_n  emp.sal%TYPE;
   v_comm_o emp.comm%TYPE;
   v_comm_n emp.comm%TYPE;
   v_ename  emp.ename%TYPE;
BEGIN
  UPDATE emp
     SET sal  = (sal * 1.05) + 100,
         comm = comm + 10
   WHERE empno = p_empno
   RETURNING  OLD sal, OLD comm, NEW sal, NEW comm, OLD ename
        INTO  v_sal_o, v_comm_o, v_sal_n, v_comm_n, v_ename;
        
   DBMS_OUTPUT.PUT_LINE('SAL changed from '||to_char(v_sal_o) ||' to '||to_char(v_sal_n)||' for '||initcap(v_ename) );     
   DBMS_OUTPUT.PUT_LINE('COMM changed from '||nvl(to_char(v_comm_o),'nothing')||' to '||nvl(to_char(v_comm_n),'nothing')||' for '||initcap(v_ename) );
END;
/

-- bulk collect into table
DECLARE
   TYPE amounts IS TABLE OF emp.sal%TYPE;
   TYPE enames  IS TABLE OF emp.ename%TYPE;
   v_sal_o  amounts;
   v_sal_n  amounts;
   v_comm_o amounts;
   v_comm_n amounts;
   v_ename  enames;
BEGIN
  UPDATE emp
     SET sal  = (sal * 1.05) + 100,
         comm = comm + 10
   WHERE deptno = 10    
   RETURNING          OLD sal, OLD comm, NEW sal, NEW comm, OLD ename
   BULK COLLECT INTO  v_sal_o, v_comm_o, v_sal_n, v_comm_n, v_ename;
        
  FOR emps IN 1..v_sal_o.COUNT LOOP        
     DBMS_OUTPUT.PUT_LINE('---- change '||to_char(emps));
     DBMS_OUTPUT.PUT_LINE('SAL changed from '||to_char(v_sal_o(emps)) ||' to '||to_char(v_sal_n(emps))||' for '||initcap(v_ename(emps)) );     
     DBMS_OUTPUT.PUT_LINE('COMM changed from '||nvl(to_char(v_comm_o(emps)),'nothing')||' to '||nvl(to_char(v_comm_n(emps)),'nothing')||' for '||initcap(v_ename(emps)) );
  END LOOP;   
END;
/

--- available for INSERT & UPDATE with NULLS for OLD (insert) respectively NEW (delete) values 


--------------------------------------
--- CASE [EXPRESSION] enhancements ---
--------------------------------------
 
CREATE TABLE cpu_load (id              NUMBER GENERATED ALWAYS AS IDENTITY,
                       percentage_load NUMBER);

INSERT INTO cpu_load (percentage_load) VALUES
  (-8),(0),(15),(46),(60),(61),(76),(80),(88),(100),(107);

SET SERVEROUTPUT ON

-- works with older versions
DECLARE
  v_status VARCHAR2(20);
BEGIN
  FOR c IN (SELECT percentage_load FROM cpu_load) LOOP
    v_status :=
    CASE 
      WHEN c.percentage_load  < 0   THEN 'impossible'
      WHEN c.percentage_load  = 60  THEN 'great'
      WHEN c.percentage_load <= 70  THEN 'ok'
      WHEN c.percentage_load  = 80  THEN 'great'
      WHEN c.percentage_load <= 85  THEN 'observe'
      WHEN c.percentage_load <= 100 THEN 'check'
      ELSE                               'impossible'
    END;
    DBMS_OUTPUT.PUT_LINE(c.percentage_load || ' : ' || v_status);
  END LOOP;
END;
/

-- 23ai: multiple choices, no left operand (dangling predicate)
DECLARE
  v_status VARCHAR2(20);
BEGIN
  FOR c IN (SELECT percentage_load FROM CPU_LOAD) LOOP
    v_status :=
    CASE c.percentage_load
      WHEN  <0, >100  THEN 'impossible'
      WHEN 60, 80     THEN 'great'
      WHEN <= 70      THEN 'ok'
      WHEN <= 85      THEN 'observe'
      ELSE                 'check'
    END;
    DBMS_OUTPUT.PUT_LINE(c.percentage_load || ' : ' || v_status);
  END LOOP;
END;
/

