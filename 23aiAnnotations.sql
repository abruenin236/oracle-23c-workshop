-------------------
--- Annotations ---
-------------------

-- DESCRIBE supports Annotations

CREATE TABLE EMP2 (
   empno    NUMBER(4,0)    ANNOTATIONS( Full_Name 'Employee ID',  Key_with_no_value), 
	ename    VARCHAR2(10)   ANNOTATIONS( Full_Name 'Employee Last Name'),
	job      VARCHAR2(9),
	mgr      NUMBER(4,0), 
	hiredate DATE, 
	sal      NUMBER(7,2)    ANNOTATIONS( Classification 'Sensitive Information'), 
	comm     NUMBER(7,2)    ANNOTATIONS( my_annotation 'I want that too'), 
	deptno   NUMBER(2,0)
) 
ANNOTATIONS (Full_Name 'Employees', useless_annotation );

SELECT object_name, object_type, column_name, annotation_name, annotation_value
   FROM user_annotations_usage
   ORDER BY annotation_name, annotation_value;

SELECT object_name, object_type, column_name, annotation_name, annotation_value
   FROM user_annotations_usage
   WHERE annotation_name = 'CLASSIFICATION'
   AND annotation_value = 'Sensitive Information'
   ORDER BY annotation_name, annotation_value;

-- if one column is sensitive, the whole table should be too
ALTER TABLE EMP2 ANNOTATIONS (Classification 'Sensitive Information');
ALTER TABLE EMP2 MODIFY (comm ANNOTATIONS (ADD Classification 'Sensitive Information'));

ALTER TABLE EMP2 ANNOTATIONS (DROP useless_annotation);
ALTER TABLE EMP2 MODIFY (comm ANNOTATIONS (DROP my_annotation));


SELECT object_name, object_type, column_name, annotation_name, annotation_value
    FROM user_annotations_usage
ORDER BY annotation_name, annotation_value;

-- Supporting the App-Developer with Metadate in JSON
SELECT column_name, JSON_ARRAYAGG(JSON_OBJECT(annotation_name, annotation_value))
FROM user_annotations_usage
WHERE object_name = 'EMP2'  AND 
      object_type = 'TABLE' AND 
      column_name IS NOT NULL
GROUP BY column_name;


