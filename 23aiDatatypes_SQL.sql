---------------------------
--- IF [NOT] EXISTS DDL ---
---------------------------


CREATE TABLE    mytable_a (id number, mytext varchar2(40));
CREATE SEQUENCE myseq_a;
CREATE VIEW     myview_a AS SELECT id, mytext FROM mytable_a;
CREATE FUNCTION myfunction_a RETURN NUMBER IS
                   v_count NUMBER;
                BEGIN
                   SELECT count(*) INTO v_count FROM myview_a;
                   RETURN v_count;
                END;
/


DROP TABLE    mytable_a;
DROP SEQUENCE myseq_a;
DROP VIEW     myview_a;
DROP FUNCTION myfunction_a;


CREATE TABLE    IF NOT EXISTS mytable_a (id number, mytext varchar2(40));
CREATE SEQUENCE IF NOT EXISTS myseq_a;
CREATE VIEW     IF NOT EXISTS myview_a AS SELECT id, mytext FROM mytable_a;
CREATE FUNCTION IF NOT EXISTS myfunction_a RETURN NUMBER IS
   v_count NUMBER;
BEGIN
   SELECT count(*) INTO v_count FROM myview_a;
   RETURN v_count;
END;
/

-- CREATE OR REPLACE and IF NOT EXISTS not allowed in same statement
CREATE OR REPLACE FUNCTION IF NOT EXISTS myfunction_a RETURN NUMBER IS
BEGIN
   RETURN null;
END;
/

-- CAUTION: if table exists, nothing happens and no error message
-- ALTER TABLE IF EXISTS does not check columns 
CREATE TABLE IF NOT EXISTS mytable_a (a NUMBER, b NUMBER, c NUMBER, d NUMBER, e NUMBER);

desc mytable_a

DROP TABLE    IF EXISTS mytable_a;
DROP SEQUENCE IF EXISTS myseq_a;
DROP VIEW     IF EXISTS myview_a;
DROP FUNCTION IF EXISTS myfunction_a;
  
-- CAUTION: typo, but no message  
DROP TABLE IF EXISTS chiesecaaake;



--------------------------------------
--- Table Values Constructor (ISO) ---
--------------------------------------

SELECT * FROM (VALUES
                  (1,'a','Text a'),
                  (2,'b','Text b'),
                  (3,'c','Text c'),
                  (4,'d','Text d')
               ) virtual_tab (id, letter, description);    
          
              
WITH virtual_tab (id, letter, description) AS
     (VALUES
          (1,'a','Text a'),
          (2,'b','Text b'),
          (3,'c','Text c'),
          (4,'d','Text d')
     )
SELECT * FROM virtual_tab;     

CREATE TABLE emp
       (empno 	 NUMBER(4) NOT NULL,
	    ename 	 VARCHAR2(10),
	    job  	 VARCHAR2(9),
	    mgr  	 NUMBER(4),
	    hiredate DATE,
	    sal    	 NUMBER(7,2),
	    comm 	 NUMBER(7,2),
	    deptno 	 NUMBER(2)
       );
       
CREATE TABLE dept
       (deptno 	NUMBER(2) NOT NULL,
	    dname 	VARCHAR2(14),
	    loc 	VARCHAR2(13) 
       );

INSERT INTO emp (empno,ename,job,mgr,hiredate,sal,comm,deptno) VALUES 
	(7369,'SMITH','CLERK',7902,to_date('17-12-80','DD-MM-YY'),800,NULL,20),
   (7499,'ALLEN','SALESMAN',7698,to_date('20-02-81','DD-MM-YY'),1600,300,30),
   (7521,'WARD','SALESMAN',7698,to_date('22-02-81','DD-MM-YY'),1250,500,30),
	(7566,'JONES','MANAGER',7839,to_date('02-04-81','DD-MM-YY'),2975,NULL,20),
	(7654,'MARTIN','SALESMAN',7698,to_date('28-09-81','DD-MM-YY'),1250,1400,30),
	(7698,'BLAKE','MANAGER',7839,to_date('01-05-81','DD-MM-YY'),2850,NULL,30),
	(7782,'CLARK','MANAGER',7839,to_date('09-06-81','DD-MM-YY'),2450,NULL,10),
	(7788,'SCOTT','ANALYST',7566,SYSDATE-85,3000,NULL,20),
	(7839,'KING','PRESIDENT',NULL,to_date('17-11-81','DD-MM-YY'),5000,NULL,10),
	(7844,'TURNER','SALESMAN',7698,to_date('08-09-81','DD-MM-YY'),1500,0,30),
	(7876,'ADAMS','CLERK',7788,SYSDATE-51,1100,NULL,20),
	(7900,'JAMES','CLERK',7698,to_date('03-12-81','DD-MM-YY'),950,NULL,30),
	(7902,'FORD','ANALYST',7566,to_date('03-12-81','DD-MM-YY'),3000,NULL,20),
	(7934,'MILLER','CLERK',7782,to_date('23-01-82','DD-MM-YY'),1300,NULL,10);
    

INSERT INTO dept (deptno, dname, loc) VALUES 
   (10,'ACCOUNTING','NEW YORK'),
   (20,'RESEARCH','DALLAS'),
   (30,'SALES','CHICAGO'),
   (40,'OPERATIONS','BOSTON');

COMMIT;


-- flexible to use
MERGE INTO emp e
   USING ( VALUES
            ( 7934, 'MILLER', 1),
            ( 7935, 'MILES', 2)
         ) src_tab (empno, ename, sal)
      ON (src_tab.empno = e.empno)
   WHEN MATCHED THEN
      UPDATE SET e.ename = src_tab.ename,
                 e.sal   = src_tab.sal
   WHEN NOT MATCHED THEN
      INSERT (e.empno, e.ename, e.sal)
      VALUES (src_tab.empno, src_tab.ename, src_tab.sal);

SELECT * FROM emp;

ROLLBACK;


------------------------------
--- Boolean Datatype (ISO) ---
------------------------------


CREATE TABLE boolean_datatype (id          NUMBER GENERATED ALWAYS AS IDENTITY,
                               bool_value1 BOOLEAN,
                               bool_value2 BOOLEAN,
                               bool_input  VARCHAR2(20));
                               
INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input) 
VALUES
   (true,false    ,'true/false'),
   (TRUE,FALSE    ,'TRUE/FALSE'), 
   (1,0           ,'1/0 as number'),
   ('true','false','true/false as String'),
   ('TRUE','FALSE','TRUE/FALSE as String'),
   ('TRuE','faLSE','TReE/faLSE as String'),
   ('yes' ,'no'   ,'yes/no as String'),
   ('YES' ,'NO'   ,'YES/NO as String'),
   ('on'  ,'off'  ,'on/off as String'),
   ('ON'  ,'OFF'  ,'ON/OFF as String'),
   ('1'   ,'0'    ,'1/0 as String'),
   ('t'   ,'f'    ,'t/f as String'),
   ('T'   ,'F'    ,'T/F as String'),
   ('y'   ,'n'    ,'y/n as String'),
   ('Y'   ,'N'    ,'Y/N as String'),
   (null,null,'null as null');
  

INSERT INTO boolean_datatype (bool_input) VALUES ('"nothing" as null');

INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input) VALUES ('ja','nein','ja/nein');

INSERT INTO boolean_datatype (bool_value1, bool_value2, bool_input)  
   SELECT to_boolean('true'), to_boolean(0),'explicit conversion'; 
   
COMMIT;
   
--- Display (1 or true) depending from version of tool/driver
SELECT * FROM boolean_datatype;

-- Only in SQL*Plus
COLUMN bool_value1 BOOLEAN yes no
COLUMN bool_value2 BOOLEAN yes no
SELECT * FROM boolean_datatype;

-- But explicit conversion available in Java based tools
SELECT true, to_char(false), to_char(bool_value1), to_number(bool_value2) FROM boolean_datatype;

SELECT * FROM boolean_datatype WHERE bool_value1 IS true;

SELECT * FROM boolean_datatype WHERE bool_value1;

SELECT * FROM boolean_datatype WHERE bool_value1 IS null;

--- in PL/SQL plsql_implicit_conversion_bool=true for overloading conversion functions
set serveroutput on

DECLARE
  v_bool    BOOLEAN := true;
  v_number  NUMBER;
BEGIN
  v_number := v_bool;
  DBMS_OUTPUT.PUT_LINE( 'Die Antwort auf alle Fragen ist: ' || v_number);
End;
/

--- in PL/SQL plsql_implicit_conversion_bool=true for overloading conversion functions
ALTER SESSION SET plsql_implicit_conversion_bool=true;
ALTER SESSION SET plsql_implicit_conversion_bool=false;

---------------------------------------
--- GROUP BY & HAVING using Aliases ---
---------------------------------------

SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY initcap(dname) || ' - (' || initcap(loc) || ')'
HAVING count(*) > 3                   
   AND sum(sal+nvl(comm,0)) < 11000;


SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY department
HAVING num_of_employees > 3                   
   AND sum_income < 11000;


SELECT initcap(dname) || ' - (' || initcap(loc) || ')' AS department, 
       count(*)                                        AS num_of_employees, 
       sum(sal+nvl(comm,0))                            AS sum_income
FROM   dept, emp
WHERE  emp.deptno = dept.deptno
GROUP BY 1                  -- reference by number allowed depending parameter
HAVING num_of_employees > 3                   
   AND sum_income < 11000;

ALTER SESSION SET group_by_position_enabled=true;
ALTER SESSION SET group_by_position_enabled=false;


--------------------------------------
--- UPDATE, DELETE -> Direct Joins ---
--------------------------------------

ALTER TABLE emp MODIFY job VARCHAR2(30);

UPDATE emp 
   SET job = job || ' (' ||loc|| ')'
  FROM dept
 WHERE emp.deptno = dept.deptno;
 -- potentially additional filters on dept table

SELECT * FROM emp;

DELETE emp 
  FROM dept
 WHERE emp.deptno = dept.deptno
   AND loc = 'DALLAS'; 
   
SELECT * FROM emp;

ROLLBACK; -- Dallas needed later


-----------------------
--- Default on Null ---
-----------------------

CREATE TABLE SHOP_BASKET(id          NUMBER GENERATED ALWAYS AS IDENTITY,
                         product     VARCHAR2(20) ,
                         weight      NUMBER       DEFAULT '0',
                         unity       VARCHAR2(10) DEFAULT ON NULL 'kg',
                         best_before DATE         DEFAULT ON NULL FOR INSERT ONLY sysdate+21,
                         color       VARCHAR2(10) DEFAULT ON NULL FOR INSERT AND UPDATE 'green');
                  
--desc shop_basket              
info shop_basket
                                  
INSERT INTO SHOP_BASKET (product, weight, unity, best_before, color) VALUES
    ('PEARS',1500,'gr',sysdate+90,'yellow');
    
INSERT INTO SHOP_BASKET (product, weight, unity, best_before, color) VALUES    
    ('APPLE',null,null,null,null);
    
INSERT INTO SHOP_BASKET (product) VALUES
    ('BROCCOLI');
    
SELECT * FROM SHOP_BASKET;    

-- DEFAULT 0, does not fire on UPDATE
UPDATE SHOP_BASKET 
   SET weight = null;
   
SELECT * FROM SHOP_BASKET;    

-- DEFAULT ON NULL 'kg', does not fire on UPDATE
UPDATE SHOP_BASKET 
   SET unity = null;
   
-- DEFAULT ON NULL FOR INSERT ONLY sysdate+21, same as above
UPDATE SHOP_BASKET 
   SET best_before = null;
   
-- DEFAULT ON NULL FOR INSERT AND UPDATE 'green'  
UPDATE SHOP_BASKET 
   SET color = null;   
   
SELECT * FROM SHOP_BASKET;


---------------------------
--- SQL ANALYSIS REPORT ---
---------------------------

SELECT d.*
FROM hr.departments d, hr.employees e 
WHERE d.department_id = e.department_id
      AND substr(phone_number,1,1) = '1'
   UNION 
SELECT d.*
FROM hr.departments d, hr.employees e, hr.jobs j
WHERE d.department_id = e.department_id
      AND substr(email,1,2) = 'GB';


SELECT * FROM table(DBMS_XPLAN.DISPLAY_CURSOR(FORMAT=>'typical'));

SELECT d.*
FROM hr.departments d, hr.employees e 
WHERE d.department_id = e.department_id
      AND substr(phone_number,1,1) = '1'
   UNION -- Leave this
SELECT d.*
FROM hr.departments d, hr.employees e, hr.jobs j
WHERE d.department_id = e.department_id
      AND e.job_id = j.job_id -- FIX
      AND email like 'GB%'; -- FIX
      
SELECT * FROM table(DBMS_XPLAN.DISPLAY_CURSOR(FORMAT=>'typical'));


-----------------------------------------
-- Subsumption of Views and Subqueries --
-----------------------------------------

SELECT *
FROM (SELECT avg(s.AMOUNT_SOLD) av
      FROM  sales s, products p
      WHERE s.prod_id = p.prod_id
      AND   s.channel_id = 4
      AND   s.promo_id = 351) v1
      ,
     (SELECT sum(p.PROD_LIST_PRICE) sm
      FROM  sales s, products p
      WHERE s.prod_id = p.prod_id
      AND   s.channel_id = 4
      AND   s.promo_id = 999) v2;


SELECT avg(CASE WHEN s.PROMO_ID = 351
         THEN s.AMOUNT_SOLD ELSE NULL END) av,
       sum(CASE WHEN s.PROMO_ID = 999
         THEN p.PROD_LIST_PRICE ELSE NULL END) sm
FROM  sales s, products p
WHERE s.prod_id = p.PROD_ID
AND   s.CHANNEL_ID = 4
AND   s.PROMO_ID IN (351,999);


----------------------------------------
--- MATERIALIZED VIEWS ON ANSI JOINS ---
----------------------------------------

CREATE MATERIALIZED VIEW empdept_mv1 AS
    SELECT * 
    FROM emp
    NATURAL JOIN dept;

CREATE MATERIALIZED VIEW empdept_mv2 AS
    SELECT * 
    FROM emp
    JOIN dept USING ( deptno );

CREATE MATERIALIZED VIEW empdept_mv3 AS
    SELECT * 
    FROM emp e
    JOIN dept d ON e.deptno = d.deptno;


