-------------------------
--- Schema Privileges ---
-------------------------

-- sys 
CREATE USER app_user IDENTIFIED BY OraclePW987$$$ QUOTA UNLIMITED ON users;
GRANT CREATE SESSION TO app_user;
CREATE ROLE myrole;    

--scott
CREATE TABLE    IF NOT EXISTS one (a NUMBER, b VARCHAR2(10));
CREATE VIEW     IF NOT EXISTS two as SELECT sysdate AS mydate;
CREATE SEQUENCE IF NOT EXISTS three;
CREATE OR REPLACE PROCEDURE four AS
BEGIN
  null;
END;  
/
INSERT INTO one values 
   (1,'Bruening'),
   (2,'Willems');

COMMIT;

GRANT SELECT  ANY TABLE     ON SCHEMA scott TO app_user;
GRANT SELECT  ANY SEQUENCE  ON SCHEMA scott TO app_user;
GRANT EXECUTE ANY PROCEDURE ON SCHEMA scott TO app_user;

GRANT SELECT  ANY TABLE     ON SCHEMA scott TO myrole;
GRANT INSERT  ANY TABLE     ON SCHEMA scott TO myrole;
GRANT UPDATE  ANY TABLE     ON SCHEMA scott TO myrole;
GRANT DELETE  ANY TABLE     ON SCHEMA scott TO myrole;

-- app_user
SELECT * FROM scott.one;
SELECT * FROM scott.two;
SELECT scott.three.nextval;
EXECUTE scott.four;

-- no INSERT granted
INSERT INTO scott.one VALUES (3,'X');

-- scott
CREATE OR REPLACE FUNCTION five RETURN VARCHAR2 AS
BEGIN
  return 'Hello World';
END;  
/
CREATE TABLE six AS SELECT * FROM one;

-- app_user
SELECT scott.five();
SELECT * FROM scott.six;

SELECT * FROM user_schema_privs;
SELECT * FROM session_schema_privs;


