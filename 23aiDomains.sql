--------------------------------------------
--- Domains - Single Point of Definition ---
--------------------------------------------

--- GRANT CREATE DOMAIN TO scott;    // included in DB_DEVELOPER_ROLE 


CREATE DOMAIN my_email_domain AS VARCHAR2(100)
   CONSTRAINT email_address_check CHECK (regexp_like (my_email_domain, '^(\S+)\@(\S+)\.(\S+)$'))
      DISPLAY 'EMAIL : '||lower(my_email_domain)
        ORDER lower(my_email_domain);


CREATE TABLE  MyColleagues (id    NUMBER GENERATED ALWAYS AS IDENTITY,
                            email DOMAIN my_email_domain );
                                         
desc MyColleagues    

SELECT * FROM user_constraints WHERE table_name = 'MYCOLLEAGUES';

INSERT INTO MyColleagues (email) VALUES ('uwe.SCHMAHLjohann@oracle.com');
INSERT INTO MyColleagues (email) VALUES ('klaus.schroeder@oracle');
INSERT INTO MyColleagues (email) VALUES 
     ('klaus.schroeder@oracle.com'),
     ('MARKUS.BRUENING@ORACLE.COM'),
     ('MANFRED.THIEM@ORACLE.com');

SELECT email FROM MyColleagues ORDER BY email;

SELECT domain_display(email) FROM MyColleagues ORDER BY domain_order(email);

-- Display domain per column
SELECT domain_name(id), domain_name(email) FROM MyColleagues WHERE ROWNUM = 1;

SELECT domain_check(my_email_domain,'andreas.gorsitzke@.com');
SELECT domain_check(my_email_domain,'andreas.gorsitzke@oracle.com');


ALTER DOMAIN my_email_domain MODIFY DISPLAY substr(lower(my_email_domain), 1 , instr(my_email_domain,'@') - 1);

SELECT domain_display(email) FROM MyColleagues ORDER BY email;

--- OBJECT_TYPE = DOMAIN
SELECT object_name, object_type FROM user_objects 
   WHERE object_name LIKE 'MY%' ORDER BY 2;

--- domain_name column in user_tab_columns
SELECT column_name, domain_name FROM user_tab_columns WHERE table_name = 'MYCOLLEAGUES';


-- alternate spelling / length

CREATE TABLE MyColleagues2 (
   id       NUMBER GENERATED ALWAYS AS IDENTITY,
   email1                  DOMAIN   my_email_domain, -- with DOMAIN keyword
   email2                           my_email_domain, -- w/o DOMAIN keyword
   email3   VARCHAR2(80)   DOMAIN   my_email_domain, -- shorten the data type, not possible if DOMAIN is defined STRICT
   email4   VARCHAR2(200)  DOMAIN   my_email_domain  -- extend the data type, not possible if DOMAIN is defined STRICT
);

SELECT column_name, domain_name FROM user_tab_columns WHERE table_name = 'MYCOLLEAGUES2';


-- domains could be multi-column

CREATE DOMAIN Full_Name AS (
   first_name  AS VARCHAR2(32),
   middle_name AS VARCHAR2(32),
   last_name   AS VARCHAR2(32)
   )
   DISPLAY  first_name || ' ' || middle_name || ' ' || last_name
   ORDER    last_name || ' ' || first_name || ' ' || middle_name
;

SELECT * FROM user_domain_cols WHERE domain_name = 'FULL_NAME' ORDER BY column_id;

-- create table with multi column domain

CREATE TABLE MyColleagues3 (
   id          NUMBER GENERATED ALWAYS AS IDENTITY,
   first_name  VARCHAR2(32),
   middle_name VARCHAR2(32),
   last_name   VARCHAR2(32),
   DOMAIN   Full_Name( first_name, middle_name, last_name )
);

SELECT column_name, domain_name FROM user_tab_columns WHERE table_name = 'MYCOLLEAGUES3';


-- Flexible Domains, requires extra domains

CREATE FLEXIBLE DOMAIN postal_address (Street, HouseNr, City, State, Postcode)
   CHOOSE DOMAIN USING (ISO_CountryCode VARCHAR2(2))
      FROM CASE ISO_CountryCode
         WHEN 'UK' then Postal_UK(HouseNr, Street, City, State, Postcode)
         WHEN 'CH' then Postal_CH(Street, HouseNr, City, State, Postcode)
         ELSE Postal_generic(Street, HouseNr, ZIPCode, City, State)
END;


--- dropping domains

--- dependent table exists
DROP DOMAIN my_email_domain;

DROP TABLE MyColleagues;

--- dependent table still exists (in recycle bin)
DROP DOMAIN my_email_domain;

PURGE RECYCLEBIN;

DROP DOMAIN my_email_domain;
DROP DOMAIN my_email_domain force; -- issue in 23.5 ?


--- Built-In Domains
SELECT * FROM all_domains ORDER BY name;

-- SELECT * FROM all_domain_cols;

-- as sys / show Built-In Domain EMAIL_D
set serveroutput on

DECLARE
    ddl_code VARCHAR2(4000);
BEGIN    
    SELECT dbms_metadata.get_ddl('SQL_DOMAIN','EMAIL_D','SYS') INTO ddl_code;
    DBMS_OUTPUT.PUT_LINE(ddl_code);
END;
/

--- ENUM DOMAINS

CREATE USECASE DOMAIN days_of_week AS ENUM ( 
  monday = 0,
  tuesday, wednesday, thursday,
  friday, saturday, sunday 
);

SELECT * FROM days_of_week;

CREATE TABLE days (day days_of_week);

INSERT INTO days VALUES 
 (days_of_week.monday), (1),
 (days_of_week.sunday);

SELECT   day FROM days;

SELECT   ENUM_NAME AS DAY_VERBOSE
FROM     days, days_of_week
WHERE    day = ENUM_VALUE;

--- OPTIONAL
--- drop force preserve
CREATE DOMAIN my_email_domain AS VARCHAR2(100)
   CONSTRAINT email_address_check CHECK (regexp_like (my_email_domain, '^(\S+)\@(\S+)\.(\S+)$'))
      DISPLAY 'EMAIL : '||lower(my_email_domain)
        ORDER lower(my_email_domain);

CREATE TABLE MyColleagues (id    NUMBER GENERATED ALWAYS AS IDENTITY,
                           email DOMAIN my_email_domain );
                                         
INSERT INTO MyColleagues (email) VALUES 
     ('uwe.SCHMAHLjohann@oracle.com'),
     ('klaus.schroeder@oracle.com'),
     ('MARKUS.BRUENING@ORACLE.COM'),
     ('MANFRED.THIEM@ORACLE.com'),
     ('andreas.gorsitzke@oracle.com');      
     

DROP DOMAIN my_email_domain FORCE PRESERVE;

desc MyColleagues    

SELECT * FROM user_constraints WHERE TABLE_NAME = 'MYCOLLEAGUES';

SELECT domain_display(email) FROM MyColleagues ORDER BY domain_order(email);

SELECT * FROM MyColleagues;

DROP TABLE MyColleagues;

--- domain evolution
CREATE DOMAIN my_email_domain AS VARCHAR2(100)
   CONSTRAINT email_address_check CHECK (regexp_like (my_email_domain, '^(\S+)\@(\S+)\.(\S+)$'))
      DISPLAY 'EMAIL : '||lower(my_email_domain)
        ORDER lower(my_email_domain);

CREATE TABLE MyColleagues (id    NUMBER GENERATED ALWAYS AS IDENTITY,
                           email DOMAIN my_email_domain );
                                         
INSERT INTO MyColleagues (email) VALUES 
     ('uwe.SCHMAHLjohann@oracle.com'),
     ('klaus.schroeder@oracle.com'),
     ('MARKUS.BRUENING@ORACLE.COM'),
     ('MANFRED.THIEM@ORACLE.com'),
     ('andreas.gorsitzke@oracle.com');     


DROP DOMAIN my_email_domain force preserve;

CREATE DOMAIN my_email_domain AS VARCHAR2(50)
   CONSTRAINT email_address_check CHECK (regexp_like ( lower(my_email_domain), '^(\S+)\@oracle.com'))
      DISPLAY lower(substr(my_email_domain,1,instr(my_email_domain,'@')-1))
        ORDER lower(my_email_domain);
        
desc MyColleagues    

SELECT * FROM user_constraints WHERE TABLE_NAME = 'MYCOLLEAGUES';        

--- Bug or feature ---> Length/Constraints
ALTER TABLE mycolleagues MODIFY email DOMAIN my_email_domain;

desc MyColleagues    

SELECT * FROM user_constraints WHERE TABLE_NAME = 'MYCOLLEAGUES';   

SELECT domain_display(email) FROM MyColleagues ORDER BY domain_order(email);

