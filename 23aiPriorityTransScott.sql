-----------------------------
--- Priority Transactions ---
-----------------------------
--- Session 1 - SCOTT ---
-------------------------

ALTER SESSION SET txn_priority = low;

UPDATE emp SET sal = 0;


SELECT * FROM emp;