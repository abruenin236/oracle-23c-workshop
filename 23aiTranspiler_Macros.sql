--------------------------------
--- Automatic SQL Transpiler ---
--------------------------------

CREATE OR REPLACE FUNCTION datetostring_f (p_hiredate DATE) RETURN VARCHAR2 IS
BEGIN
   RETURN to_char(p_hiredate,'YYYY.MM.DD');
END;
/

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'ON';

SELECT count(*)
  WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

ALTER SESSION SET sql_transpiler = 'OFF';


------------------
--- SQL Macros ---
------------------


-- It's a new, simpler way to encapsulate complex processing logic directly within SQL. 
-- SQL Macros allow developers to encapsulate complex processing within a new structure called a "macro",
--- which can then be used within SQL statement. 
-- Essentially there two types of SQL Macros: SCALAR and TABLE. 
--- SCALAR expressions can be used in SELECT list, WHERE/HAVING, GROUP BY/ORDER BY clauses  
--- TABLE expressions used in a FROM-clause

/* -- SCALAR Macro (vs. Function)
CREATE OR REPLACE FUNCTION datetostring_f (p_hiredate DATE) RETURN VARCHAR2 IS
BEGIN
  RETURN to_char(p_hiredate,'YYYY.MM.DD');
END;
/ */

-- see type SCALAR - default is TABLE
CREATE OR REPLACE FUNCTION datetostring_m (p_hiredate DATE) RETURN VARCHAR2 sql_macro(scalar) IS
BEGIN
  RETURN q'{to_char(p_hiredate,'YYYY.MM.DD')}';
END;
/

SELECT to_char(hiredate,'YYYY'), datetostring_f(hiredate), datetostring_m(hiredate) FROM emp; 

SELECT count(*) 
 WHERE datetostring_f ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;
 
SELECT * FROM dbms_xplan.display_cursor();

SELECT count(*)
 WHERE datetostring_m ( sysdate + level ) > '2022'
 CONNECT BY LEVEL <= 2000000;

SELECT * FROM dbms_xplan.display_cursor();

--- TABLE Macro
CREATE OR REPLACE FUNCTION total_salary(department VARCHAR2 DEFAULT NULL,
                                        job        VARCHAR2 DEFAULT NULL) 
   RETURN CLOB SQL_MACRO IS
BEGIN
  RETURN q'{
   SELECT d.deptno, d.dname, e.job, sum(e.sal) AS SalSummary 
     FROM emp e, dept d
    WHERE e.deptno=d.deptno
      AND d.dname = coalesce(total_salary.department,d.dname)
      AND e.job   = coalesce(total_salary.job,e.job)
     GROUP BY d.deptno, d.dname, e.job
  }';
END;
/
 
SELECT * FROM total_salary('RESEARCH','CLERK'); 

SELECT * FROM total_salary('RESEARCH');

SELECT * FROM total_salary(job=>'CLERK');

SELECT * FROM total_salary();



--- optional
CREATE OR REPLACE FUNCTION myselect 
    (n NUMBER, 
     t dbms_tf.table_t,
     c dbms_tf.columns_t) 
  RETURN VARCHAR2 SQL_MACRO IS
BEGIN
   RETURN 'SELECT * FROM t ORDER BY ' || c(1) || ' FETCH FIRST n ROWS ONLY';
END;
/

select * from myselect(3,emp,columns(ename));

select * from myselect(2,dept,columns(loc));

select * from myselect(10,user_objects,columns(object_name));



CREATE TABLE meeting_attendees (  
  attendee_id  NUMBER NOT NULL,  
  start_date   DATE NOT NULL,  
  end_date     DATE,  
  PRIMARY KEY( attendee_id, start_date )  
)
/

INSERT INTO meeting_attendees VALUES 
    ( 1, trunc(sysdate) + 9/24, trunc(sysdate) + 10/24 ),
    ( 1, trunc(sysdate) + 10/24, trunc(sysdate) + 10.5/24 ),
    ( 1, trunc(sysdate) + 11/24, trunc(sysdate) + 13/24 ),
    ( 1, trunc(sysdate) + + 16/24, trunc(sysdate) + 17/24 ),
    ( 2, trunc(sysdate) + 9/24, trunc(sysdate) + 12/24 ),
    ( 2, trunc(sysdate) + 12.5/24, trunc(sysdate) + 13/24 ),
    ( 2, trunc(sysdate) + 13/24, trunc(sysdate) + 14/24 ),
    ( 2, trunc(sysdate) + 15/24, trunc(sysdate) + 17/24 );

SELECT * FROM meeting_attendees  
ORDER BY attendee_id, start_date, end_date
/

SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')
  FROM meeting_attendees MATCH_RECOGNIZE (  
    ORDER BY start_date, end_date  
    MEASURES  
      max ( end_date ) start_gap,   
      next ( start_date ) end_gap
    ALL ROWS PER MATCH   
    PATTERN ( ( gap | {-no_gap-} )+ )  
    DEFINE gap AS max ( end_date ) < next ( start_date )  
  )
/

CREATE OR REPLACE FUNCTION fg (  
  tab            dbms_tf.table_t,   
  date_cols      dbms_tf.columns_t  
)  
  RETURN VARCHAR2   
  SQL_MACRO   
AS  
BEGIN  
  RETURN 'tab MATCH_RECOGNIZE (   
             ORDER BY ' || fg.date_cols ( 1 ) || ', ' || fg.date_cols ( 2 ) || '    
             MEASURES     
               max ( ' || fg.date_cols ( 2 ) || ' ) start_gap,   
               next ( ' || fg.date_cols ( 1 ) || ' ) end_gap  
               ALL ROWS PER MATCH  
               PATTERN ( ( gap | {-no_gap-} )+ )    
               DEFINE     
                  gap AS  max ( ' || fg.date_cols ( 2 ) || ' ) < (   
                  next ( ' || fg.date_cols ( 1 ) || ' )  
                  )    
              )';  
END fg; 
/

SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')  
FROM   fg (   
  meeting_attendees,   
  COLUMNS ( start_date, end_date )   
)
/

CREATE TABLE other_table (  
  person     VARCHAR2(10) NOT NULL,  
  timebegin  DATE,  
  timeend    DATE 
)
/

INSERT INTO other_table VALUES 
    ( 'Rainer', trunc(sysdate) + 9/24, trunc(sysdate) + 10/24 ),
    ( 'Rainer', trunc(sysdate) + 10/24, trunc(sysdate) + 10.5/24 ),
    ( 'Rainer', trunc(sysdate) + 11/24, trunc(sysdate) + 13/24 ),
    ( 'Rainer', trunc(sysdate) + 16/24, trunc(sysdate) + 17/24 ),
    ( 'Marcel', trunc(sysdate) + 12.5/24, trunc(sysdate) + 13/24 ),
    ( 'Marcel', trunc(sysdate) + 13/24, trunc(sysdate) + 14/24 ),
    ( 'Marcel', trunc(sysdate) + 15/24, trunc(sysdate) + 17/24 );  


SELECT to_char(start_gap, 'dd.mm.yy hh24:mi'), to_char(end_gap, 'dd.mm.yy hh24:mi')  
FROM   fg (   
  other_table,   
  COLUMNS ( timebegin, timeend )   
)
/


