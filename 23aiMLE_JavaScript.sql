------------------------------------
--- JavaScript Stored Procedures ---
------------------------------------

--- multilingual_engine initialization parameter set to enable
--- included in db_developer_role
GRANT CREATE MLE TO scott;  
GRANT EXECUTE ON JAVASCRIPT TO scott;
--- GRANT EXECUTE DYNAMIC MLE TO <role | user>   --- DBMS_MLE


CREATE OR REPLACE MLE MODULE myJS_module
LANGUAGE javascript AS 

   function string2obj(inputString) {
       let myObject = {};

       const kvPairs = inputString.split(";");
       kvPairs.forEach( pair => {
           const tuple = pair.split("=");
           if ( tuple.length === 1 ) {
                tuple[1] = false;
              } else if ( tuple.length != 2 ) {
                throw "parse error: you need to use exactly one '=' between key and value and not use '=' in either key or value";
              }
           myObject[tuple[0]] = tuple[1];
       });
       return myObject;
   }

   function concat(str1, str2) {
        return str1 + str2;
   }

   export { string2obj, concat }
   export function js_multiply(p1, p2) { return p1*p2; }
/ 

CREATE OR REPLACE FUNCTION mystring(p_str varchar2) RETURN JSON
   AS MLE MODULE myJS_module
      SIGNATURE 'string2obj(string)';
/    

CREATE OR REPLACE FUNCTION myconcat(p_str1 VARCHAR2, p_str2 VARCHAR2) RETURN VARCHAR2
   AS MLE MODULE myJS_module 
      SIGNATURE 'concat(string,string)';
/    

CREATE OR REPLACE FUNCTION mymultiply(p_1 NUMBER, p_2 NUMBER) RETURN NUMBER
   AS MLE MODULE myJS_module 
      SIGNATURE 'js_multiply(number,number)';
/    

SELECT json_serialize(mystring('a=1;b=2;c=3;d') PRETTY );

SELECT myconcat('Ha','llo');
 
SELECT mymultiply(42,2);   


--- Inline Usage
CREATE OR REPLACE FUNCTION mystring2("inputString" VARCHAR2) RETURN JSON
AS MLE LANGUAGE javascript 
{{
    let myObject = {};

    const kvPairs = inputString.split(";");
    kvPairs.forEach( pair => {
        const tuple = pair.split("=");
        if ( tuple.length === 1 ) {
            tuple[1] = false;
        } else if ( tuple.length != 2 ) {
            throw "parse error: you need to use exactly one '=' between key and value and not use '=' in either key or value";
        }
        myObject[tuple[0]] = tuple[1];
    });
    return myObject;
}};
/

SELECT json_serialize(mystring2('a=1;b=2;c=3;d') PRETTY );

SELECT * FROM user_source ORDER BY type, name, line;

SELECT * FROM dict WHERE table_name LIKE '%MLE%';

SELECT * FROM user_mle_procedures;

SELECT * FROM user_mle_modules;



#########################
### Use ready Modules ###
#########################
-- https://blogs.oracle.com/developers/post/how-to-import-javascript-es-modules-in-23c-free-and-use-them-in-sql-queries

-- create the MLE module named chance_module
CREATE DIRECTORY IF NOT EXISTS mle_dir AS '/home/oracle/Demo/mle';

CREATE OR REPLACE MLE MODULE chance_module LANGUAGE javascript VERSION '1.1.11' USING BFILE( mle_dir, 'chance.js' );
/

-- create the corresponding MLE environment that will help for managing depencies at runtime
-- MLE environments complement MLE modules and allow you to do the following:
-- * Set language options to customize the JavaScript runtime in its execution context
-- * Enable specific MLE modules to be imported
-- * Manage name resolution and the import chain
CREATE OR REPLACE MLE ENV chance_module_env IMPORTS ('chance' module chance_module);

SET VERIFY OFF
SET DEFINE OFF
-- create a new 'extended' module that will export properly the functions for future usage in PL/SQL
CREATE OR REPLACE MLE MODULE chance_extended LANGUAGE javascript Version '1.1.11' AS
import Chance from 'chance';
const chance = new Chance();
const chanceTypes = Object.keys(Object.getPrototypeOf(chance));

// inspired from Fony
function valid(type) {
  return chanceTypes.indexOf(type) !== -1;
}

function getArrayValue(definition) {
  if (definition.length !== 2) {
    return null;
}
  const type = definition[0];
  const count = definition[1];
  if (!valid(type) || typeof count !== "number" || count === 0) {
    return null;
}
  return new Array(count).fill(null).map(function() {
    return getValue(type);
  });
}

function getValue(type) {
  if (Array.isArray(type)) {
    return getArrayValue(type);
  }
  if (typeof type === "object" && !Array.isArray(type) && type != null) {
    var result = template(type);
    return result._$_chance ? result._$_chance : result;
  }
  try {
    return chance[type]();
  } catch (exception) {
    return null;
  }
}

function template(json_template) {
  const output = {};
  Object.keys(json_template).map(function(key, index) {
    if (typeof key === "string" && key.charAt(0) === '$' && valid(key.substring(1))) {
      output['_$_chance'] = chance[key.substring(1)](json_template[key]);
    } else {
      output[key] = getValue(json_template[key]);
    }
  });
  return output;
}

chance.yearStr = chance.year;
chance.year = function (doc) { return parseInt(chance.yearStr(doc)); }

export {chance, template};
/


-- Installing a PL/SQL package to make Chance functions accessible in SQL
-- See how the chance_module_env MLE environment is being used so that dependencies resolution goes well
CREATE OR REPLACE PACKAGE chance AS
 function address( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.address';
 function address return varchar2 as mle module chance_extended env chance_module_env signature 'chance.address';
 function age return number as mle module chance_extended env chance_module_env signature 'chance.age';
 function age( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.age';
 function altitude return number as mle module chance_extended env chance_module_env signature 'chance.altitude';
 function altitude( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.altitude';
 function ampm( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ampm';
 function ampm return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ampm';
 function android_id( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.android_id';
 function android_id return varchar2 as mle module chance_extended env chance_module_env signature 'chance.android_id';
 function animal( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.animal';
 function animal return varchar2 as mle module chance_extended env chance_module_env signature 'chance.animal';
 function apple_token( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.apple_token';
 function apple_token return varchar2 as mle module chance_extended env chance_module_env signature 'chance.apple_token';
 function areacode return varchar2 as mle module chance_extended env chance_module_env signature 'chance.areacode';
 function areacode( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.areacode';
 function avatar( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.avatar';
 function avatar return varchar2 as mle module chance_extended env chance_module_env signature 'chance.avatar';
 function bb_pin return varchar2 as mle module chance_extended env chance_module_env signature 'chance.bb_pin';
 function bb_pin( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.bb_pin';
 function birthday( p_doc in json ) return date as mle module chance_extended env chance_module_env signature 'chance.birthday';
 function birthday return date as mle module chance_extended env chance_module_env signature 'chance.birthday';
 function bool( p_doc in json ) return boolean as mle module chance_extended env chance_module_env signature 'chance.bool';
 function bool return boolean as mle module chance_extended env chance_module_env signature 'chance.bool';
 function cc return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cc';
 function cc( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cc';
 function cc_type return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cc_type';
 function cc_type( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cc_type';
 function cf( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cf';
 function cf return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cf';
 function character return varchar2 as mle module chance_extended env chance_module_env signature 'chance.character';
 function character( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.character';
 function city( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.city';
 function city return varchar2 as mle module chance_extended env chance_module_env signature 'chance.city';
 function coin( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.coin';
 function coin return varchar2 as mle module chance_extended env chance_module_env signature 'chance.coin';
 function color( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.color';
 function color return varchar2 as mle module chance_extended env chance_module_env signature 'chance.color';
 function company return varchar2 as mle module chance_extended env chance_module_env signature 'chance.company';
 function company( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.company';
 function country return varchar2 as mle module chance_extended env chance_module_env signature 'chance.country';
 function country( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.country';
 function county return varchar2 as mle module chance_extended env chance_module_env signature 'chance.county';
 function county( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.county';
 function cpf return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cpf';
 function cpf( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.cpf';
 function currency return json as mle module chance_extended env chance_module_env signature 'chance.currency';
 function currency( p_doc in json ) return json as mle module chance_extended env chance_module_env signature 'chance.currency';
 function currency_pair( p_doc in json ) return json as mle module chance_extended env chance_module_env signature 'chance.currency_pair';
 function currency_pair return json as mle module chance_extended env chance_module_env signature 'chance.currency_pair';
 function d10( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d10';
 function d10 return number as mle module chance_extended env chance_module_env signature 'chance.d10';
 function d100 return number as mle module chance_extended env chance_module_env signature 'chance.d100';
 function d100( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d100';
 function d12 return number as mle module chance_extended env chance_module_env signature 'chance.d12';
 function d12( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d12';
 function d20( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d20';
 function d20 return number as mle module chance_extended env chance_module_env signature 'chance.d20';
 function d30( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d30';
 function d30 return number as mle module chance_extended env chance_module_env signature 'chance.d30';
 function d4( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d4';
 function d4 return number as mle module chance_extended env chance_module_env signature 'chance.d4';
 function d6 return number as mle module chance_extended env chance_module_env signature 'chance.d6';
 function d6( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d6';
 function d8 return number as mle module chance_extended env chance_module_env signature 'chance.d8';
 function d8( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.d8';
 function "date" return date as mle module chance_extended env chance_module_env signature 'chance.date';
 function "date"( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.date';
 function "date"( p_doc in json, p_will_be_date in boolean ) return date as mle module chance_extended env chance_module_env signature 'chance.date';
 function depth return number as mle module chance_extended env chance_module_env signature 'chance.depth';
 function depth( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.depth';
 function dollar return varchar2 as mle module chance_extended env chance_module_env signature 'chance.dollar';
 function dollar( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.dollar';
 function domain( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.domain';
 function domain return varchar2 as mle module chance_extended env chance_module_env signature 'chance.domain';
 function email return varchar2 as mle module chance_extended env chance_module_env signature 'chance.email';
 function email( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.email';
 function euro return varchar2 as mle module chance_extended env chance_module_env signature 'chance.euro';
 function euro( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.euro';
 function exp return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp';
 function exp( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp';
 function exp_month( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp_month';
 function exp_month return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp_month';
 function exp_year( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp_year';
 function exp_year return varchar2 as mle module chance_extended env chance_module_env signature 'chance.exp_year';
 function falsy return varchar2 as mle module chance_extended env chance_module_env signature 'chance.falsy';
 function falsy( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.falsy';
 function fbid return varchar2 as mle module chance_extended env chance_module_env signature 'chance.fbid';
 function fbid( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.fbid';
 function first( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.first';
 function first return varchar2 as mle module chance_extended env chance_module_env signature 'chance.first';
 function floating( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.floating';
 function floating return number as mle module chance_extended env chance_module_env signature 'chance.floating';
 function gender return varchar2 as mle module chance_extended env chance_module_env signature 'chance.gender';
 function gender( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.gender';
 function geohash return varchar2 as mle module chance_extended env chance_module_env signature 'chance.geohash';
 function geohash( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.geohash';
 function geojson return json as mle module chance_extended env chance_module_env signature 'chance.geojson';
 function geojson( p_doc in json ) return json as mle module chance_extended env chance_module_env signature 'chance.geojson';
 function google_analytics( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.google_analytics';
 function google_analytics return varchar2 as mle module chance_extended env chance_module_env signature 'chance.google_analytics';
 function guid( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.guid';
 function guid return varchar2 as mle module chance_extended env chance_module_env signature 'chance.guid';
 function hammertime return number as mle module chance_extended env chance_module_env signature 'chance.hammertime';
 function hammertime( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.hammertime';
 function hash return varchar2 as mle module chance_extended env chance_module_env signature 'chance.hash';
 function hash( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.hash';
 function hashtag( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.hashtag';
 function hashtag return varchar2 as mle module chance_extended env chance_module_env signature 'chance.hashtag';
 function hour( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.hour';
 function hour return number as mle module chance_extended env chance_module_env signature 'chance.hour';
 function iban return varchar2 as mle module chance_extended env chance_module_env signature 'chance.iban';
 function iban( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.iban';
 function "integer" return number as mle module chance_extended env chance_module_env signature 'chance.integer';
 function "integer"( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.integer';
 function ip( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ip';
 function ip return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ip';
 function ipv6( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ipv6';
 function ipv6 return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ipv6';
 function klout( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.klout';
 function klout return number as mle module chance_extended env chance_module_env signature 'chance.klout';
 function last return varchar2 as mle module chance_extended env chance_module_env signature 'chance.last';
 function last( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.last';
 function latitude return number as mle module chance_extended env chance_module_env signature 'chance.latitude';
 function latitude( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.latitude';
 function letter( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.letter';
 function letter return varchar2 as mle module chance_extended env chance_module_env signature 'chance.letter';
 function locale return varchar2 as mle module chance_extended env chance_module_env signature 'chance.locale';
 function locale( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.locale';
 function longitude( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.longitude';
 function longitude return number as mle module chance_extended env chance_module_env signature 'chance.longitude';
 function millisecond return number as mle module chance_extended env chance_module_env signature 'chance.millisecond';
 function millisecond( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.millisecond';
 function minute( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.minute';
 function minute return number as mle module chance_extended env chance_module_env signature 'chance.minute';
 function month( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.month';
 function month return varchar2 as mle module chance_extended env chance_module_env signature 'chance.month';
 function name return varchar2 as mle module chance_extended env chance_module_env signature 'chance.name';
 function name( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.name';
 function natural( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.natural';
 function natural return number as mle module chance_extended env chance_module_env signature 'chance.natural';
 function normal( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.normal';
 function normal return number as mle module chance_extended env chance_module_env signature 'chance.normal';
 function paragraph( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.paragraph';
 function paragraph return varchar2 as mle module chance_extended env chance_module_env signature 'chance.paragraph';
 function phone return varchar2 as mle module chance_extended env chance_module_env signature 'chance.phone';
 function phone( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.phone';
 function postal return varchar2 as mle module chance_extended env chance_module_env signature 'chance.postal';
 function postal( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.postal';
 function postcode return varchar2 as mle module chance_extended env chance_module_env signature 'chance.postcode';
 function postcode( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.postcode';
 function prefix( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.prefix';
 function prefix return varchar2 as mle module chance_extended env chance_module_env signature 'chance.prefix';
 function prime( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.prime';
 function prime return number as mle module chance_extended env chance_module_env signature 'chance.prime';
 function profession return varchar2 as mle module chance_extended env chance_module_env signature 'chance.profession';
 function profession( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.profession';
 function province( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.province';
 function province return varchar2 as mle module chance_extended env chance_module_env signature 'chance.province';
 function radio( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.radio';
 function radio return varchar2 as mle module chance_extended env chance_module_env signature 'chance.radio';
 function rpg( p_doc in varchar2 ) return json as mle module chance_extended env chance_module_env signature 'chance.rpg';
 function second( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.second';
 function second return number as mle module chance_extended env chance_module_env signature 'chance.second';
 function sentence return varchar2 as mle module chance_extended env chance_module_env signature 'chance.sentence';
 function sentence( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.sentence';
 function ssn return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ssn';
 function ssn( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.ssn';
 function state return varchar2 as mle module chance_extended env chance_module_env signature 'chance.state';
 function state( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.state';
 function street( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.street';
 function street return varchar2 as mle module chance_extended env chance_module_env signature 'chance.street';
 function string( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.string';
 function string return varchar2 as mle module chance_extended env chance_module_env signature 'chance.string';
 function suffix return varchar2 as mle module chance_extended env chance_module_env signature 'chance.suffix';
 function suffix( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.suffix';
 function syllable( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.syllable';
 function syllable return varchar2 as mle module chance_extended env chance_module_env signature 'chance.syllable';
 function timestamp return number as mle module chance_extended env chance_module_env signature 'chance.timestamp';
 function timestamp( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.timestamp';
 function timezone return json as mle module chance_extended env chance_module_env signature 'chance.timezone';
 function timezone( p_doc in json ) return json as mle module chance_extended env chance_module_env signature 'chance.timezone';
 function tld return varchar2 as mle module chance_extended env chance_module_env signature 'chance.tld';
 function tld( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.tld';
 function tv( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.tv';
 function tv return varchar2 as mle module chance_extended env chance_module_env signature 'chance.tv';
 function twitter( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.twitter';
 function twitter return varchar2 as mle module chance_extended env chance_module_env signature 'chance.twitter';
 function url( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.url';
 function url return varchar2 as mle module chance_extended env chance_module_env signature 'chance.url';
 function word return varchar2 as mle module chance_extended env chance_module_env signature 'chance.word';
 function word( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.word';
 function wp7_anid( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.wp7_anid';
 function wp7_anid return varchar2 as mle module chance_extended env chance_module_env signature 'chance.wp7_anid';
 function wp8_anid2( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.wp8_anid2';
 function wp8_anid2 return varchar2 as mle module chance_extended env chance_module_env signature 'chance.wp8_anid2';
 function year( p_doc in json ) return number as mle module chance_extended env chance_module_env signature 'chance.year';
 function year return number as mle module chance_extended env chance_module_env signature 'chance.year';
 function zip( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.zip';
 function zip return varchar2 as mle module chance_extended env chance_module_env signature 'chance.zip';
 function zodiac( p_doc in json ) return varchar2 as mle module chance_extended env chance_module_env signature 'chance.zodiac';
 function zodiac return varchar2 as mle module chance_extended env chance_module_env signature 'chance.zodiac';
 function template( p_doc in json ) return json as mle module chance_extended env chance_module_env signature 'template';
end;
/

-- Generate a random string
SELECT chance.string CONNECT BY LEVEL <= 5;

-- Generate a random string with 5 chars
SELECT chance.string( json { 'length': 5 } ) CONNECT BY LEVEL <= 5;

-- Generate 7 random strings with 10 chars
SELECT chance.string( json { 'length': 10 } ) CONNECT BY LEVEL <= 5;

-- Generate 5 prime numbers (between 0 to 10000)
SELECT chance.prime CONNECT BY LEVEL <= 5;

-- Generate a random credit card number (Mastercard)
SELECT chance.cc( json { 'type': 'Mastercard' } ) CONNECT BY LEVEL <= 5;

-- Generate 2 random dates (which includes time for Oracle database)
-- first one is a DATE, second one is a formatted string (or varchar2)
SELECT chance."date", to_char( chance."date", 'yyyy-mm-dd hh24:mi:ss' ) CONNECT BY LEVEL <= 5;

-- Generate a random date as a string (or varchar2)
-- here, the format is the one from Chance: american (28th of January 2024, MM/DD/YYYY)
SELECT chance."date"( json{ 'string': true } ) CONNECT BY LEVEL <= 5;

-- Generate a random date as a string (or varchar2), but this time with format DD/MM/YYYY
SELECT chance."date"( json{ 'string': true, 'american': false } ) CONNECT BY LEVEL <= 5;

-- Generate a random address
SELECT chance.address CONNECT BY LEVEL <= 5;

-- Generate a random timezone
SELECT chance.timezone CONNECT BY LEVEL <= 5;


-- If you've taken a look at the extended MLE module above, you may have seen some additional code. 
-- In fact, while looking for JavaScript data generators, I found out that Fony.js (MIT License) from [Safia Abdalla]
-- (https://github.com/captainsafia) provided an interesting concept of `JSON template`. 
-- So I've decided to integrate it and improve it to benefit from the additional possibilities offered by `Chance.js` in terms of 
-- generator customization using a JSON document as a parameter. The result is that the `template()` function can now be used to 
-- generate random JSON documents based on JSON templates!

-- Generate a JSON document containing tags as an array of 3 words
SELECT chance.template( json {'tags': ['word', 3]} ) CONNECT BY LEVEL <= 5;

-- Generate a JSON document containing random information for a person
SELECT chance.template( json { 'name': 'name', 'age': 'age', 'address': 'address' } ) CONNECT BY LEVEL <= 5;


-- Generate 3 random JSON documents for fictious races
-- We display the JSON document formatted for better readability
SELECT json_serialize( chance.template( json {'raceId': { '$natural': {'min': 1, 'max': 99999} }, 
                                              'name': 'name', 
                                              'laps': { '$natural': {'min': 5, 'max': 42} }, 
                                              'date': 'date', 
                                              'podium': {} } ) pretty ) AS new_races 
  CONNECT BY LEVEL <= 5;


