--------------------------------
--- Sessionless Transactions ---
--------------------------------

-- grant EXECUTE ON SYS.DBMS_TRANSACTION to scott;

drop table if exists test;
create table test( id number, text varchar2(100) );

-- Starting a New Sessionless Transaction on the Server with a User-Specified GTRID
SET SERVEROUTPUT ON

DECLARE
  gtrid VARCHAR2(128);
BEGIN
  gtrid := DBMS_TRANSACTION.START_TRANSACTION
  ( UTL_RAW.CAST_TO_RAW('SessionLessTest')
  , DBMS_TRANSACTION.TRANSACTION_TYPE_SESSIONLESS
  , 180
  , DBMS_TRANSACTION.TRANSACTION_NEW
  );
END;
/

-- check transaction ID
exec DBMS_OUTPUT.PUT_LINE('>>> ' || UTL_RAW.CAST_TO_VARCHAR2(DBMS_TRANSACTION.GET_TRANSACTION_ID()) || ' <<<' );

-- do some stuff
insert into test values (1, 'One');

select * from test;

-- suspend transaction
exec DBMS_TRANSACTION.SUSPEND_TRANSACTION;

select * from test;

exec DBMS_OUTPUT.PUT_LINE('>>> ' || UTL_RAW.CAST_TO_VARCHAR2(DBMS_TRANSACTION.GET_TRANSACTION_ID()) || ' <<<' );

-- resume the transaction in separate session

select * from test;

DECLARE
    gtrid VARCHAR2(128);
BEGIN
    gtrid := DBMS_TRANSACTION.START_TRANSACTION
    ( UTL_RAW.CAST_TO_RAW('SessionLessTest')
    , DBMS_TRANSACTION.TRANSACTION_TYPE_SESSIONLESS
    , 180
    , DBMS_TRANSACTION.TRANSACTION_RESUME
    );
END;
/

-- check transaction state

select * from test;

set SERVEROUTPUT ON
exec DBMS_OUTPUT.PUT_LINE('>>> ' || UTL_RAW.CAST_TO_VARCHAR2(DBMS_TRANSACTION.GET_TRANSACTION_ID()) || ' <<<' );

-- add more stuff and commit
insert into test values (2, 'Two');

select * from test;

commit;

-- back in local transaction, data still there
select * from test;


